import {Alert} from 'react-bootstrap';

const AlertSuccess = ({ titleHead, titleBody, titlePie, show }) => show ?  <Alert variant="danger">
    <Alert.Heading>{titleHead}</Alert.Heading>
    <p>
        {titleBody}
    </p>
    <hr />
    <p className="mb-0">
        {titlePie}
    </p>
    </Alert> : null

export default AlertSuccess;
