import React from 'react';
import {BrowserRouter, Switch, Route} from 'react-router-dom';
import LinkRouters from '../constants/linkRoutes';
import Home from '../app/home';
import Login from '../pages/auth/login';
import SumForm from '../pages/sumForm/sumForm';


function Routes() {
    return (
  <BrowserRouter>
    <Switch>
      <Route exact path={LinkRouters.login} component={Login}/>
      <Route exact path={LinkRouters.home} component={Home}/>
      <Route exact path={LinkRouters.sumForm} component={SumForm}/>
    </Switch>
  </BrowserRouter>)
}

export default Routes;