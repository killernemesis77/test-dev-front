import React from 'react'
import '../../assets/css/login.css';
import '../../../node_modules/bootstrap/dist/css/bootstrap.min.css'
import ReactDOM, { render } from 'react-dom';
import Home from '../../app/home';

const Login = () => {
    const expressions = {
        user: new RegExp(/^[a-zA-Z]{4,20}$/), // Letras, numeros, guion y guion_bajo
        password: (/^.{4,12}$/), // 4 a 12 digitos.
    }
    const state={
        form:{
            idUsuario: ''
            ,idContrasena: ''
        }
    }
    
    const obteneridentificador=async e=>{     
        var idControl = e.target.id;
        if (idControl === 'idUsuario') 
        state.form.idUsuario = e.target.value;
        else if (idControl === 'idContrasena') 
        state.form.idContrasena = e.target.value;
    }    

    const autenticacion=()=> {
        try {
            const URL= 'https://localhost:44309/api/Auth';
            var obj = {
                user_name: state.form.idUsuario,
                password: state.form.idContrasena
            }
            const requestOptions = {
                method: 'POST',
                headers: { 'Content-Type': 'application/json' },
                body: JSON.stringify({ user_name: obj.user_name, password: obj.password})
            };
            fetch(URL, requestOptions)
            .then(async (response)=>{                
                const data = await response.json();
                if (obj.idUsuario === data.user_name && obj.idContrasena === data.password
                    && data.id_permits_system_tbs === 1 && data.id_state_system_tbs === 1) {
                        alert("Bienvenido:" + data.user_name);
                        renderHome();
                } else alert("Usuario o Contraseña incorrectos.");
            })
            .catch((error) =>{
                console.log("Error al consumir servicio: " + error);
            })                            
        } catch (error) {
            console.log("error #CONST-FN-AUTH:" + error);
        }        
    }

    const renderHome=()=> {
        const element = <Home/>;
            ReactDOM.render(
            element,
            document.getElementById('root')
        );
    }

    return <>
        <div>
            <br/>
            {/* ---- *.*.*.*.*.*.*.*.*.*.*.* ----
                ---- SECTION PAGE ----
            ..... */} 
                <section className="login-block">
                <div className="container">
                <div className="row">
                <div className="col-md-4 banner-sec">
                <div className="signup__overlay"></div>
                <div className="banner">
                <div id="demo" className="carousel slide carousel-fade" data-ride="carousel">
                <ol className="carousel-indicators">
                <li data-target="#demo" data-slide-to="0" className="active"></li>
                <li data-target="#demo" data-slide-to="1"></li>
                </ol>
                <div className="carousel-inner">
                <div className="carousel-item active">
                  <img src="@/assets/images/footer_logo.png" className="img-fluid"/>
                <h1>Test Development</h1>
                </div>
                <div className="carousel-item">
                  <img src="@/assets/images/footer_logo.png" className="img-fluid"/>
                <h1>Prueba desarrollo Andres Leyton</h1>
                </div>
                </div>
                </div>
                </div>
                </div>
                <div className="col-md-8 login-sec">
                <h2 className="text-center">Login</h2>
                <form className="login100-form validate-form">
                {/* ---- *.*.*.*.*.*.*.*.*.*.*.* ----
                ---- INPUT USER FORM ----
                ..... */} 
                <div className="wrap-input100 validate-input">
                <span className="label-input100">Usuario</span>
                <input className="input100" type="text" id="idUsuario" onChange={obteneridentificador} minLength={4} maxLength={20} placeholder="Ingresa tú usuario"/>
                <span className="focus-input100"></span>
                </div>
                {/* ---- *.*.*.*.*.*.*.*.*.*.*.* ----
                ---- INPUT PASSWORD FORM ----
                ..... */} 
                <div className="wrap-input100 validate-input">
                <span className="label-input100">Contraseña</span>
                <input className="input100" type="password" id="idContrasena" onChange={obteneridentificador} minLength={4} maxLength={12} placeholder="Ingresa tú contraseña"/>
                <span className="focus-input100 password"></span>
                </div>

                <div className="text-right p-t-8 p-b-31">
                <a >
                ¿Olvido su contraseña?
                </a>
                </div>

                <div className="container-login100-form-btn">
                <div className="wrap-login100-form-btn">
                <button className="btn btn-purple mr-2"
                //  disabled={!expressions.user.test(state.form.idUsuario)}
                onClick={renderHome}>
                Ingresar
                </button>
                  <button  className="btn btn-purple mr-2">
                Nuevo Usuario
                </button>
                </div>
                </div>

                <div className="txt1 text-center p-t-54 p-b-20">
                <span>
                O regístrate usando
                </span>
                </div>

                <div className="flex-c-m">
                <a href="#" className="login100-social-item bg1">
                <i className="fa fa-facebook"></i>
                </a>
                <a href="#" className="login100-social-item bg3">
                <i className="fa fa-google"></i>
                </a>
                </div>
                </form>

                <div className="copy-text">Andres Leyton™
                </div>
                </div>
                </div>
                </div>
                </section>
            {/* ---- *.*.*.*.*.*.*.*.*.*.*.* ----
            ---- END PAGE ----
            ..... */}                                             
            </div>
    </>;
}

export default Login;