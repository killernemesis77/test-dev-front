import React from 'react'
import '../../assets/css/sumForm.css';
import '../../../node_modules/bootstrap/dist/css/bootstrap.min.css'
import ReactDOM from 'react-dom';
import Home from '../../app/home';
// import {AlertSuccess} from '../../components/AlertSuccess.js';

const SumForm = () => {
    const expressions = {
        user: new RegExp(/^[a-zA-Z]{4,20}$/), // Letras, numeros, guion y guion_bajo
        password: (/^.{4,12}$/), // 4 a 12 digitos.
    }
    const state={
        form:{
            idPrimerValor: 0
            ,idSegundoValor: 0
            ,idResultadoOperacion: 0
        }
    }
    
    const obteneridentificador=async e=>{     
        try {
            var idControl = e.target.id;
            if (idControl === 'idPrimerValor') 
            state.form.idPrimerValor = e.target.value;
            else if (idControl === 'idSegundoValor') 
            state.form.idSegundoValor = e.target.value;
        } catch (error) {
            console.log("Error al obtener identificador de campos: " + error);
        }
    }

    const valideCamps=()=> {
        try {
            if (parseInt(state.form.idPrimerValor) === 0 && parseInt(state.form.idSegundoValor) === 0) alert("Los campos son obligatorios");
            else if (parseInt(state.form.idPrimerValor) > 0 && parseInt(state.form.idSegundoValor) === 0) alert("El campo Segundo valor es obligatorio");
            else if (parseInt(state.form.idPrimerValor) === 0 && parseInt(state.form.idSegundoValor) > 0) alert("El campo Primer valor es obligatorio");
            else return true;
        } catch (error) {
            console.log("Error al momento de validar campos: " + error);
        }              
    }

    const sumValues=()=> {
        try {
            if (!valideCamps()) return true;
            var objData = {
                firstValue: parseInt(state.form.idPrimerValor),
                secondValue: parseInt(state.form.idSegundoValor),
                result: 0
            }
            objData.result = objData.firstValue + objData.secondValue;
            alert("El resultado de la suma es: " + objData.result);
            // setDataAlert({titleHead: 'Operacion suma', titleBody: 'El resultado es: ' + objData.result, titlePie: 'suma +', show: true});            
        } catch (error) {
            console.log("Error, se presento una inconsistencia al realizar la operación: " + error);
        }
    }

    const renderHome=()=> {
        try {
            const element = <Home/>;
            ReactDOM.render(
            element,
            document.getElementById('root')
        );   
        } catch (error) {
          console.log("Error al renderizar HOME: " + error);
        }        
    }

    return <>
        <div>
            {/* ---- *.*.*.*.*.*.*.*.*.*.*.* ----
                ---- SECTION PAGE ----
            ..... */} 
                <section className="login-block">
                <div className="container">
                <div className="row">
                <div className="col-md-4 banner-sec">
                <div className="signup__overlay"></div>
                <div className="banner">
                <div id="demo" className="carousel slide carousel-fade" data-ride="carousel">
                <ol className="carousel-indicators">
                <li data-target="#demo" data-slide-to="0" className="active"></li>
                <li data-target="#demo" data-slide-to="1"></li>
                </ol>
                <div className="carousel-inner">
                <div className="carousel-item active">
                  <img src="@/assets/images/footer_logo.png" className="img-fluid"/>
                <h1>Test Development</h1>
                </div>
                <div className="carousel-item">
                  <img src="@/assets/images/footer_logo.png" className="img-fluid"/>
                <h1>Prueba login Andres Leyton</h1>
                </div>
                </div>
                </div>
                </div>
                </div>
                <div className="col-md-8 login-sec">
                <h2 className="text-center">Sumar</h2>
                <form className="login100-form validate-form">
                {/* ---- *.*.*.*.*.*.*.*.*.*.*.* ----
                ---- INPUT USER FORM ----
                ..... */} 
                <div className="wrap-input100 validate-input">
                <span className="label-input100">Primer Valor</span>
                <input className="input100" type="numeric" id="idPrimerValor" onChange={obteneridentificador} minLength={1} maxLength={20} placeholder="Ingrese el primer valor"/>
                <span className="focus-input100"></span>
                </div>
                {/* ---- *.*.*.*.*.*.*.*.*.*.*.* ----
                ---- INPUT PASSWORD FORM ----
                ..... */} 
                <div className="wrap-input100 validate-input">
                <span className="label-input100">Segundo Valor</span>
                <input className="input100" type="numeric" id="idSegundoValor" onChange={obteneridentificador} minLength={1} maxLength={12} placeholder="Ingrese el segundo valor"/>
                <span className="focus-input100 password"></span>
                </div>
  
                <div className="container-login100-form-btn">
                <div className="wrap-login100-form-btn">
                <button className="btn btn-purple mr-2"
                //  disabled={!expressions.user.test(state.form.idUsuario)}
                onClick={sumValues}>
                Sumar
                </button>
                <button  onClick={renderHome} className="btn btn-purple mr-2">
                Cancelar
                </button>
                </div>
                </div>              
                </form>

                <div className="copy-text">Andres Leyton™
                </div>
                </div>
                </div>
                </div>
                </section>
            {/* ---- *.*.*.*.*.*.*.*.*.*.*.* ----
            ---- END PAGE ----
            ..... */}                                             
            </div>
    </>;
}

export default SumForm;