import React from 'react';
import ReactDOM from 'react-dom';
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css';
import {Navbar
    , Nav
    , NavDropdown} from 'react-bootstrap';
import LinkRouters from '../constants/linkRoutes';

const Home=()=>{
    return <>
    <div>
        <Navbar bg="success" expand="lg">
          <Navbar.Brand href="#home">Test Developer</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="mr-auto">
              <Nav.Link href="#home">Home</Nav.Link>
              <NavDropdown title="Opciones" id="basic-nav-dropdown">
                <NavDropdown.Item href={LinkRouters.sumForm}>Sumar numeros</NavDropdown.Item>
                <NavDropdown.Item href={LinkRouters.Home}>Inicio</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href={LinkRouters.login}>Cerrar sesión</NavDropdown.Item>
              </NavDropdown>
            </Nav>     
          </Navbar.Collapse>
        </Navbar>                                        
    </div>
</>;
}

export default Home;